<?php

abstract class Documento
{
    protected $número;

    abstract public function eValido();

    abstract public function formata();

    public function setNúmero($número)
    {
        $this-> número = preg_replace('/[^0-9]/', '', $número);
    }

    public function getNúmero()
    {
        return $this-> número;
    }
}


class CPF extends Documento
{
    public function __construct($número)
    {
        $this-> setNúmero($número);
    }
    public function eValido()
    {
    }

    public function formata()
    {
        //Formato do CPF: ###.###.###-###
        return substr($this-> número, 0, 3) . '.' .
               substr($this-> número, 3, 3) . '.' .
               substr($this-> número, 6, 3) . '-' .
               substr($this-> número, 9, 2);
    }
}


class CNPJ extends Documento
{
    public function __construct($número)
    {
        $this-> Setnúmero($número);
    }
    public function eValido()
    {
    }

    public function formata()
    {
    }
}


class CNH extends Documento
{
    private $categoria;

    public function __construct($número, $categoria)
    {
        $this-> Setnúmero($número);
        $this-> categoria = $categoria;
    }
    public function eValido()
    {
    }

    public function formata()
    {
    }

    public function setCategoria($categoria)
    {
        $this-> categoria = $categoria;
    }

    public function getCategoria()
    {
        return $this-> categoria;
    }
}

$cpf = new CPF('115.858.758-88');
echo $cpf-> formata();

$cpf-> setNúmero('CPF: 545.855.565-85');

echo '<br>';

echo $cpf-> Getnúmero();

echo '<br>';

$cnpjSenac = new CNPJ('03.709.814/005-65');
echo $cnpjSenac -> getNúmero();

echo '<br>';

$cnhFunalo = new CNH('1255656', 'AB');
echo $cnhFunalo-> getNúmero() .' Cat.:' . $cnhFunalo-> getCategoria();
